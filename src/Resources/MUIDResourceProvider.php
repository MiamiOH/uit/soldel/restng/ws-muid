<?php

namespace MiamiOH\WSMUID\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;
use MiamiOH\WSMUID\Services\MUID\Service;

class MUIDResourceProvider extends ResourceProvider
{
    private $name = 'muid';
    private $application = 'WebServices';
    private $module = 'MUID';

    public function registerDefinitions(): void
    {
        $this->addDefinition([
            'name' => $this->name . '.model',
            'type' => 'object',
            'properties' => [
                'pidm' => [
                    'type' => 'string',
                ],
                'bannerId' => [
                    'type' => 'string',
                ],
                'uniqueId' => [
                    'type' => 'string',
                ],
            ]
        ]);

        $this->addDefinition([
            'name' => $this->name . '.collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/' . $this->name . '.model'
            ]
        ]);
    }

    public function registerServices(): void
    {
        $this->addService([
            'name' => $this->name,
            'class' => Service::class,
            'description' => 'Provide database handler',
            'set' => [
                'dataSource' => ['type' => 'service', 'name' => 'APIDataSource']
            ]
        ]);
    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'description' => 'Translate Miami University IDs',
            'name' => $this->name . '.get',
            'service' => $this->name,
            'method' => 'get',
            'pattern' => '/muid/v1',
            'isPageable' => true,
            'defaultPageLimit' => 100,
            'maxPageLimit' => 500,
            'options' => [
                'muid' => [
                    'type' => 'list',
                    'description' => 'list of pidms, banner ids, unique ids'
                ],
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'A collection of ID records.',
                    'returns' => [
                        'type' => 'array',
                        '$ref' => '#/definitions/' . $this->name . '.collection',
                    ]
                ],
                App::API_NOTFOUND => [
                    'description' => 'There is no record found.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => $this->application,
                    'module' => $this->module,
                    'key' => ['view', 'all']
                ],
            ]
        ]);
    }

    public function registerOrmConnections(): void
    {

    }
}
