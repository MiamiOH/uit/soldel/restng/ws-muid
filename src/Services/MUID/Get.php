<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 9/26/18
 * Time: 10:39 AM
 */

namespace MiamiOH\WSMUID\Services\MUID;

use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\WSMUID\Collections\MUIDCollection;
use MiamiOH\WSMUID\Repositories\MUIDRepository;

class Get
{
    /**
     * @param Request $request
     * @param Response $response
     * @param MUIDRepository $repository
     * @return Response
     * @throws \Exception
     */
    public function getIDs(
        Request $request,
        Response $response,
        MUIDRepository $repository
    ): Response
    {
        $payLoad = [];

        $status = \MiamiOH\RESTng\App::API_OK;

        $options = $request->getOptions();

        $muids = $options['muid'] ?? [];

        $muidCollection = MUIDCollection::fromArray($muids);

        if ($muidCollection->count() == 0) {
            $response->setPayload($payLoad);
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        $offset = $request->getOffset() - 1;
        $limit = $request->getLimit();

        $payLoad = $repository->readMUIDs(
            $muidCollection,
            $offset,
            $limit
        );

        // DONE
        $response->setTotalObjects(count($payLoad));
        $response->setPayload($payLoad);
        $response->setStatus($status);
        return $response;
    }
}
