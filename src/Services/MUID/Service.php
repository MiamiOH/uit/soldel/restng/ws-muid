<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 9/24/18
 * Time: 3:05 PM
 */

namespace MiamiOH\WSMUID\Services\MUID;

use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\RESTng\Legacy\DataSource;
use MiamiOH\WSMUID\Repositories\MUIDRepositorySQL;
use MiamiOH\WSMUID\Repositories\MUIDRepository;

class Service extends \MiamiOH\RESTng\Service
{
    protected $dataSource = 'MUWS_GEN_PROD';

    /**
     * @var Request
     */
    private $request = null;

    /**
     * @var Response
     */
    private $response = null;

    /**
     * @var User
     */
    protected $user = null;

    /**
     * @var MUIDRepository
     */
    protected $repository = null;

    public function setDataSource(DataSource $dataSourceManager)
    {

    }

    public function getDependencies()
    {
        $this->user = $this->getApiUser();
        $this->response = $this->getResponse();
        $this->request = $this->getRequest();
        $this->repository = new MUIDRepositorySQL();
    }

    /**
     * @throws \Exception
     */
    public function get()
    {
        $this->getDependencies();

        $getService = new Get();

        $response = $getService->getIDs(
            $this->request,
            $this->response,
            $this->repository
        );

        return $response;
    }
}
