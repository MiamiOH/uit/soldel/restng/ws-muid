<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 1/18/19
 * Time: 4:24 PM
 */

namespace MiamiOH\WSMUID\Repositories;


use MiamiOH\WSMUID\Collections\MUIDCollection;

interface MUIDRepository
{
    /**
     * @param MUIDCollection $muidCollection
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function readMUIDs(MUIDCollection $muidCollection, int $offset, int $limit): array;
}
