<?php

namespace MiamiOH\WSMUID\Repositories;

use MiamiOH\WSMUID\Collections\MUIDCollection;
use MiamiOH\WSMUID\EloquentModels\UniqueIdentification;

/**
 * Class MUIDRepositorySQL
 */
class MUIDRepositorySQL implements MUIDRepository
{
    /**
     * @var array
     */
    protected $fieldMap = [
        'szbuniq_unique_id' => 'uniqueId',
        'szbuniq_pidm' => 'pidm',
        'szbuniq_banner_id' => 'bannerId',
    ];

    /**
     * @param MUIDCollection $muidCollection
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function readMUIDs(
        MUIDCollection $muidCollection,
        int $offset,
        int $limit
    ): array
    {
        $query = UniqueIdentification::select('szbuniq_pidm', 'szbuniq_banner_id', 'szbuniq_unique_id');

        $query->wherePidms($muidCollection->getPidms());

        $query->whereBannerIds($muidCollection->getBannerIds());

        $query->whereUniqueIds($muidCollection->getUniqueIds());

        $records = $query->limit($limit)->offset($offset)->get();

        $payLoad = $this->transformRecords($records->toArray(), $muidCollection);

        return $payLoad;
    }

    /**
     * @param array $records
     * @param MUIDCollection $muidCollection
     * @return array
     */
    private function transformRecords(array $records, MUIDCollection $muidCollection){
        $result = [];

        foreach ($records as $record) {
            $matchedIDs = [];

            // Loop through group of 3 ids (pidm, bannerId, uniqueId)
            // Change keys and check if there is a matched id
            foreach ($record as $key => $id) {
                // Change keys
                $record[$this->fieldMap[$key]] = $id;
                unset($record[$key]);

                // Check if there is a matched id in 3 ids
                if ($muidCollection->have($id)) {
                    $matchedIDs[] = $id;
                }
            }

            // Populate matched id
            foreach ($matchedIDs as $id) {
                $result[$id] = $record;
            }
        }

        return $result;
    }
}
