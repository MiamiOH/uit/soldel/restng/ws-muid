<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 1/28/19
 * Time: 7:26 PM
 */

namespace MiamiOH\WSMUID\Collections;


class MUIDCollection implements \Countable
{
    private $pidmRegex = '/^\d{1,8}$/';
    private $bannerIdRegex = '/^\+\d{1,8}$/';
    private $uniqueIdRegex = '/^\w{1,8}$/';

    private $pidms = [];
    private $bannerIds = [];
    private $uniqueIds = [];

    private $trackedIds = [];

    public function getPidms(): array
    {
        return $this->pidms;
    }

    public function getBannerIds(): array
    {
        return $this->bannerIds;
    }

    public function getUniqueIds(): array
    {
        return $this->uniqueIds;
    }

    public function count(): int
    {
        return count($this->pidms) + count($this->bannerIds) + count($this->uniqueIds);
    }

    public function addMuid(string $muid): void
    {
        $muid = strtoupper(trim($muid));

        if (preg_match($this->pidmRegex, $muid)) {
            $this->pidms[] = $muid;
            $this->trackedIds[$muid] = $muid;
        } elseif (preg_match($this->bannerIdRegex, $muid)) {
            $this->bannerIds[] = $muid;
            $this->trackedIds[$muid] = $muid;
        } elseif (preg_match($this->uniqueIdRegex, $muid)) {
            $this->uniqueIds[] = $muid;
            $this->trackedIds[$muid] = $muid;
        }
    }

    public function have(string $muid) {
        return isset($this->trackedIds[$muid]);
    }

    public static function fromArray(array $data): self
    {
        $muidCollection = new self();

        foreach ($data as $muid) {
            $muidCollection->addMuid($muid);
        }

        return $muidCollection;
    }
}