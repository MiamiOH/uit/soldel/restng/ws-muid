<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/1/18
 * Time: 8:39 PM
 */

namespace MiamiOH\WSMUID\EloquentModels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yajra\Oci8\Query\OracleBuilder;

class UniqueIdentification extends Model
{
    protected $connection = 'MUWS_GEN_PROD';


    /**
     * @var string $table Table name
     */
    public $table = 'szbuniq';

    /**
     * @var bool $timestamps Do not populate auto-generated date fields
     */
    public $timestamps = false;

    /**
     * @var bool $incrementing Do not increment primary key by default
     */
    public $incrementing = false;

    /**
     * @var string $primaryKey Primary key of table
     */
    protected $primaryKey = 'szbuniq_pidm';

    /**
     * @var array $guarded black list of insertable fields
     */
    protected $guarded = [];

    /**
     * Get a new query builder instance for the connection.
     * https://github.com/yajra/laravel-oci8/issues/73#issuecomment-117131744
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function newBaseQueryBuilder()
    {
        $conn = $this->getConnection();

        $grammar = $conn->getQueryGrammar();

        return new OracleBuilder($conn, $grammar, $conn->getPostProcessor());
    }

    /**
     * @param Builder $query
     * @param array $pidms
     * @return Builder
     */
    public function scopeWherePidms(Builder $query, array $pidms): Builder
    {
        if (!empty($pidms)) {
            $query->orWhereIn('szbuniq_pidm', $pidms);
        }
        return $query;
    }

    /**
     * @param Builder $query
     * @param array $bannerIds
     * @return Builder
     */
    public function scopeWhereBannerIds(Builder $query, array $bannerIds): Builder
    {
        if (!empty($bannerIds)) {
            $query->orWhereIn('szbuniq_banner_id', $bannerIds);
        }
        return $query;
    }

    /**
     * @param Builder $query
     * @param array $uniqueIds
     * @return Builder
     */
    public function scopeWhereUniqueIds(Builder $query, array $uniqueIds): Builder
    {
        if (!empty($uniqueIds)) {
            $query->orWhereIn('szbuniq_unique_id', $uniqueIds);
        }
        return $query;
    }

    /**
     * Oracle Limit
     *
     * @param Builder $query
     * @param int $limit
     * @return Builder
     */
    public function scopeLimit(Builder $query, int $limit = -1): Builder
    {
        if ($limit !== -1) {
            $query->raw("FETCH NEXT $limit ROWS ONLY");
        }
        return $query;
    }

    /**
     * Oracle offset
     *
     * @param Builder $query
     * @param int $offset
     * @return Builder
     */
    public function scopeOffset(Builder $query, int $offset = -1): Builder
    {
        if ($offset !== -1) {
            $query->raw("OFFSET $offset ROWS");
        }
        return $query;
    }
}
